package com.vivenat.juan.vivenat;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.MatrixCursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.vivenat.juan.vivenat.interfaces.LaboratoriosApi;
import com.vivenat.juan.vivenat.models.Laboratorio;
import com.vivenat.juan.vivenat.models.Laboratorios;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class listaProductoActivity extends ListActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    String[] columnasBD = new String[]{"_id", "imagen", "textoSuperior", "textoInferior"};
    MatrixCursor cursor = new MatrixCursor(columnasBD);

    //TODO: ESTO CAMBIARÁ CUANDO ESTÉN LISTOS LOS PRODUCTOS
    public Laboratorios productos = new Laboratorios();
    private static Context listaProductoContext;
    private String api_url;
    private ListView productosLV;
    private EditText textoCambio;

    SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto_lista);


        //dirección
        api_url = getResources().getString(R.string.api_url);
        listaProductoContext = this;

        //se crea archivo para extraer el json.
        Gson gson = new GsonBuilder().create();

        //se hace la petición.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api_url)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();


        LaboratoriosApi laboratoriosApi = retrofit.create(LaboratoriosApi.class);

        // extrae lista Laboratorios
        final Call<List<Laboratorio>> laboratorioCall = (Call<List<Laboratorio>>) laboratoriosApi.getLaboratorios();


        //encolar, para adaptar a lista de laboratorio. o error por tiempo.
        laboratorioCall.enqueue(new Callback<List<Laboratorio>>() {


            @Override
            public void onResponse(Call<List<Laboratorio>> call, Response<List<Laboratorio>> response) {


                productos.setLaboratorios(response.body()) ;
                productosLV = (ListView) getListView();
                ArrayAdapter<Laboratorio> adapter = new productoListAdapter();
                productosLV.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Laboratorio>> call, Throwable t) {
                //Log.i("error ", t.getMessage());

                Toast.makeText(getApplicationContext(), "No se pudo enlazar con el servidor. Verifica tu conexión", Toast.LENGTH_LONG).show();
            }
        });


        //Simulamos que extraemos los datos de la base de datos a un cursor

        //Añadimos los datos al Adapter y le indicamos donde dibujar cada dato en la fila del Layout

        String[] desdeEstasColumnas = {"imagen", "textoSuperior", "textoInferior"};

        int[] aEstasViews = {R.id.imageView_imagen, R.id.textView_nombre, R.id.textView_usos};

        adapter = new SimpleCursorAdapter(this, R.layout.plantilla_producto, cursor, desdeEstasColumnas, aEstasViews, 0);

        ListView listado = getListView();

        listado.setAdapter(adapter);


        ImageView tips = (ImageView) findViewById(R.id.tips);
        ImageView laboratorio = (ImageView) findViewById(R.id.laboratorio);
        //ImageView test = (ImageView) findViewById(R.id.test);



        textoCambio = (EditText) findViewById(R.id.inputSearch);

        final LinearLayout barrainferior = (LinearLayout) findViewById(R.id.bottomTabBar);

        textoCambio.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                if(hasFocus){

                    barrainferior.setVisibility(View.GONE);

                }else
                    barrainferior.setVisibility(View.VISIBLE);
            }
        });

        textoCambio.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do your stuff here
                    textoCambio.clearFocus();
                    barrainferior.setVisibility(View.VISIBLE);

                }
                return false;
            }
        });

        //Listening to button event
        tips.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), tipsActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        //Listening to button event
        laboratorio.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), listaLaboratorioActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        /*Listening to button event
        test.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), testActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });
        */

    }


    @Override
    public void onListItemClick(ListView lista, View view, int posicion, long id) {
        // Hacer algo cuando un elemento de la lista es seleccionado
        TextView textoTitulo = (TextView) view.findViewById(R.id.textView_nombre);

        CharSequence texto = textoTitulo.getText();
        //Toast.makeText(getApplicationContext(), texto, Toast.LENGTH_LONG).show();*/
        Intent nextScreen = new Intent(getApplicationContext(), productosActivity.class);
        nextScreen.putExtra("nombre", texto);
        //nextScreen.putExtra("descripcion",);
        startActivity(nextScreen);


    }


    private class productoListAdapter extends ArrayAdapter<Laboratorio> {

        public productoListAdapter() {
            super(listaProductoActivity.this, R.layout.plantilla_laboratorio, productos.getLaboratorios());
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.plantilla_producto, parent, false);
            Laboratorio laboratorioActual = productos.getLaboratorios().get(position);

            TextView name = (TextView) view.findViewById(R.id.textView_nombre);
            name.setText(laboratorioActual.getNombre());

            TextView attention = (TextView) view.findViewById(R.id.textView_usos);
            attention.setText(laboratorioActual.getDescripcion());


            ImageView image = (ImageView) view.findViewById(R.id.imageView_imagen);
            //image.setImageResource(R.drawable.artrifit);

            Picasso.with(getApplicationContext()).load(api_url + laboratorioActual.getImagen().substring(1)).into(image);

            return view;
        }
    }
}


 /*public void searchItemText(String caracter)
    {//eso recorre la cosa :')
        MatrixCursor cursortemp = new MatrixCursor(columnasBD);
        try {
            //aquí se presta para buscar por patología y nombre
            int firstNameColIndex = cursor.getColumnIndex("textoSuperior");
            //int lastNameColIndex = cursor.getColumnIndex("lastname");
            int key=0;
            while (cursor.moveToNext()) {

                if (!cursor.getString(firstNameColIndex).contains(caracter))
                {
                    Log.d("caracter", String.valueOf(caracter));
                    Log.d("indices",String.valueOf(cursor.getColumnIndex("textoSuperior")));
                    Log.d("contenido",cursor.getString(firstNameColIndex));

                }
                else
                {
                    String id=String.valueOf(key);
                    String imagen = retornoValor("imagen");
                    String nombre=retornoValor("textoSuperior");
                    String descripcion =retornoValor("textoInferior");


                    cursortemp.addRow(new Object[]{id, R.drawable.artrifit, nombre, descripcion});
                }
                // String fullName = cursor.getString(firstNameColIndex);
                // Log.d("matriz",fullName);
            }
            cursor=cursortemp;
            ListView listado = getListView();

            listado.setAdapter(adapter);
            //adapter.notifyDataSetChanged();
        }

        finally

        {
            cursor.close();
        }


    }

    public String retornoValor(String indiceMatriz)
    {

        int temp = cursor.getColumnIndex(indiceMatriz);
        return cursor.getString(temp);


    }*/





