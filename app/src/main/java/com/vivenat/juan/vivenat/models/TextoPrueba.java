package com.vivenat.juan.vivenat.models;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Decard on 9/26/16.
 */

public class TextoPrueba {

    public static ArrayList<String> getInfo(String numRegistro, String descripcion, String composicion, String dosificacion, String usos, String contraindicaciones, String tiendas)
    {


        ArrayList <String> arregloPrueba = new ArrayList<String>();


        arregloPrueba.add(dosificacion);
        arregloPrueba.add(usos);
        arregloPrueba.add(numRegistro);
        arregloPrueba.add(descripcion);
        arregloPrueba.add(composicion);
        arregloPrueba.add(contraindicaciones);
        arregloPrueba.add(tiendas);


        return arregloPrueba;
    }

    public static ArrayList<String> getTitulo ()
    {


        ArrayList <String> arregloTitulo = new ArrayList<String>();


        arregloTitulo.add("Dosificación");
        arregloTitulo.add("Usos");
        arregloTitulo.add("Número de registro");
        arregloTitulo.add("Descripción");
        arregloTitulo.add("Composición");
        arregloTitulo.add("Contraindicaciones");
        arregloTitulo.add("Dónde conseguirlo");


        return arregloTitulo;
    }



}
