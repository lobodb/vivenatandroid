package com.vivenat.juan.vivenat;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.vivenat.juan.vivenat.interfaces.LaboratoriosApi;
import com.vivenat.juan.vivenat.models.Laboratorio;
import com.vivenat.juan.vivenat.models.Laboratorios;
import com.vivenat.juan.vivenat.models.Producto;
import com.vivenat.juan.vivenat.utilities.Paginador;

import static com.vivenat.juan.vivenat.R.drawable.productos;


public class listaLaboratorioActivity extends ListActivity {

    public Laboratorios laboratorios = new Laboratorios();
    private String api_url;
    ListView laboratoriosLV;
    public List<Laboratorio> laboratoriosquellegan;
    private int pagina = 0;
    private int numeropaginas = 0;
    static ImageButton atras;
    static ImageButton adelante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laboratorio_lista);

        //dirección
        api_url = getResources().getString(R.string.api_url);

        //se crea archivo para extraer el json.
        Gson gson = new GsonBuilder().create();

        //se hace la petición.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api_url)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();


        LaboratoriosApi laboratoriosApi = retrofit.create(LaboratoriosApi.class);

        // extrae lista Laboratorios
        final Call<List<Laboratorio>> laboratorioCall = (Call<List<Laboratorio>>) laboratoriosApi.getLaboratorios();


        //encolar, para adaptar a lista de laboratorio. o error por tiempo.
        laboratorioCall.enqueue(new Callback<List<Laboratorio>>() {


            @Override
            public void onResponse(Call<List<Laboratorio>> call, Response<List<Laboratorio>> response) {
                {   Paginador paginador = new Paginador();
                    laboratoriosquellegan = response.body();

                    if (laboratoriosquellegan!=null) {
                    numeropaginas = calculadornumeropaginas(response.body().size());
                    ToggleButtonsNavigation(0, numeropaginas);
                    List<Laboratorio> laboratoriosamostrar = paginador.generarPaginaLaboratorio(0, response.body());
                    laboratorios.setLaboratorios(laboratoriosamostrar) ;
                    laboratoriosLV = (ListView) getListView();
                    ArrayAdapter<Laboratorio> adapter = new laboratoriosListAdapter();
                    laboratoriosLV.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Laboratorio>> call, Throwable t) {
                //Log.i("error ", t.getMessage());
                Toast.makeText(getApplicationContext(), "No se pudo enlazar con el servidor. Verifica tu conexión", Toast.LENGTH_LONG).show();

            }
        });

        EditText textoCambio = (EditText) findViewById(R.id.inputSearch);

       /* final LinearLayout barrainferior = (LinearLayout) findViewById(R.id.bottomTabBar);

        textoCambio.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                if(hasFocus){

                    barrainferior.setVisibility(View.GONE);

                }else
                    barrainferior.setVisibility(View.VISIBLE);
            }
        });

        textoCambio.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do your stuff here
                    barrainferior.setVisibility(View.VISIBLE);
                }
                return false;
            }
        }); */

        ImageView home = (ImageView) findViewById(R.id.home);
        ImageView tips = (ImageView) findViewById(R.id.tips);
        ImageView back = (ImageView) findViewById(R.id.atras);
        atras = (ImageButton) findViewById(R.id.botonAtras);
        adelante = (ImageButton) findViewById(R.id.botonAdelante);
        atras.setEnabled(false);
        adelante.setEnabled(false);




        //Botonera de la barra inferior.
        tips.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), tipsActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });


        home.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), homeActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        back.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), homeActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });


        atras.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Paginador paginador = new Paginador();
                pagina = pagina - 1;
                ToggleButtonsNavigation(pagina, numeropaginas);
                Log.i("productos que llegan ",laboratoriosquellegan.size()+"");
                List<Laboratorio> productosamostrar = paginador.generarPaginaLaboratorio(pagina,laboratoriosquellegan);
                laboratorios.setLaboratorios(productosamostrar); ;
                laboratoriosLV = (ListView) getListView();
                ArrayAdapter<Laboratorio> adapter = new listaLaboratorioActivity.laboratoriosListAdapter();
                laboratoriosLV.setAdapter(adapter);
            }
        });

        adelante.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Paginador paginador = new Paginador();
                pagina = pagina + 1;
                ToggleButtonsNavigation(pagina, numeropaginas);
                List<Laboratorio> productosamostrar = paginador.generarPaginaLaboratorio(pagina,laboratoriosquellegan);
                laboratorios.setLaboratorios(productosamostrar); ;
                laboratoriosLV = (ListView) getListView();
                ArrayAdapter<Laboratorio> adapter = new listaLaboratorioActivity.laboratoriosListAdapter();
                laboratoriosLV.setAdapter(adapter);
            }
        });



        /*test.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), testActivity.class);
                finish();
                startActivity(nextScreen);

            }

        });
        */



    }

    public static int calculadornumeropaginas(int entrada){
        int numeropaginas = 0;

        if(entrada>5){
            if(entrada%5 > 0){
                numeropaginas = ( entrada/5 ) + 1;
            }
            else{
                numeropaginas = ( entrada/5 );
            }

        }
        else{
            return 1;
        }



        return numeropaginas;
    }

    public static void ToggleButtonsNavigation(int pagina, int cantitaddepaginas){
        Log.i( "pagina", pagina + "" );
        if(pagina == 0) {
            atras.setEnabled(false);
        }else{
            atras.setEnabled(true);
        }

        if( (pagina + 1) == cantitaddepaginas ){
            adelante.setEnabled(false);
        }else{
            adelante.setEnabled(true);
        }
    }


    //item laboratorio aquí se pasa información.
    @Override
    public void onListItemClick(ListView lista, View view, int posicion, long id) {
        Intent nextScreen = new Intent(getApplicationContext(), laboratorioSingularActivity.class);
        nextScreen.putExtra("id_lab", laboratorios.getLaboratorios().get(posicion).getId());
        nextScreen.putExtra("nombre_lab", laboratorios.getLaboratorios().get(posicion).getNombre());
        startActivity(nextScreen);
    }

    //custom adapter.
    private class laboratoriosListAdapter extends ArrayAdapter<Laboratorio> {
        public laboratoriosListAdapter() {
            super(listaLaboratorioActivity.this, R.layout.plantilla_laboratorio, laboratorios.getLaboratorios());
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.plantilla_laboratorio, parent, false);
            Laboratorio laboratorioActual = laboratorios.getLaboratorios().get(position);

            TextView name = (TextView) view.findViewById(R.id.textView_nombre);
            name.setText(laboratorioActual.getNombre());

            TextView attention = (TextView) view.findViewById(R.id.textView_descripcion);
            attention.setText(laboratorioActual.getDescripcion());


            ImageView image = (ImageView) view.findViewById(R.id.imageView_imagen);
            //image.setImageResource(R.drawable.artrifit);

            Picasso.with(getApplicationContext()).load(api_url + laboratorioActual.getImagen().substring(1)).into(image);

            return view;
        }
    }


}
