package com.vivenat.juan.vivenat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class disclaimerActivity extends AppCompatActivity {

    TextView imei;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclaimer);

        ImageView login = (ImageView) findViewById(R.id.loginButton);

        TextView disclaimer = (TextView) findViewById(R.id.textDisclaimer);

        imei = (TextView) findViewById(R.id.usuario);

        StringBuilder sb = new StringBuilder();

        TelephonyManager telMan = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        //sb.append("Imei: "+telMan.getDeviceId()+"\n");

        imei.setText("DISCLAIMER.");

        disclaimer.setText("-La información de certificación que se garantiza es la del INVIMA en Colombia \n\n- La información contenida sobre composición, dosificación y usos está elaborada por el laboratorio de cada producto. \n\n- Vive Nat no se hace responsable por los efectos que los medicamentos puedan causar a los consumidores y no reemplaza las visitas a especialistas de medicina tradicional."
        );

        //Listening to button event
        login.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), homeActivity.class);

                finish();
                startActivity(nextScreen);

            }
        });

    }
}
