package com.vivenat.juan.vivenat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.vivenat.juan.vivenat.interfaces.ProductosApi;
import com.vivenat.juan.vivenat.models.Laboratorio;
import com.vivenat.juan.vivenat.models.Producto;
import com.vivenat.juan.vivenat.models.TextoPrueba;
import com.vivenat.juan.vivenat.models.Tienda;
import com.vivenat.juan.vivenat.utilities.DescripcionAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.vivenat.juan.vivenat.R.string.api_url;

public class productosActivity extends Activity {

    ArrayList< String> infoProducto;
    ArrayList<String> lista;
    ExpandableListView lista_Exp;
    DescripcionAdapter adapter;
    private String api_url;
    public Producto producto = new Producto();
    ImageView imagenPro;
    String id_prod;
    Context context;
    TextView nombreTitulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto_singular);

        context = this;
        id_prod = getIntent().getExtras().getString("id_prod");
        Log.i("id_prod", id_prod);

        //barra abajo
        ImageView home = (ImageView) findViewById(R.id.home);
        ImageView tips = (ImageView) findViewById(R.id.tips);
        ImageView laboratorio = (ImageView) findViewById(R.id.laboratorio);
        ImageView back = (ImageView) findViewById(R.id.atras);
        nombreTitulo = (TextView) findViewById(R.id.nombreProducto);


        imagenPro = (ImageView) findViewById(R.id.imgProducto);

        api_url = "http://vivenat.com/";


        //se crea archivo para extraer el json.
        Gson gson = new GsonBuilder().create();

        //se hace la petición.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api_url)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();


        ProductosApi productosApi = retrofit.create(ProductosApi.class);

        

        // extrae lista Laboratorios
        final Call<Producto> productoCall = (Call<Producto>) productosApi.getProducto(id_prod);

        productoCall.enqueue(new Callback<Producto>() {


            @Override
            public void onResponse(Call<Producto> call, Response<Producto> response) {

                producto = response.body();
                String nombreProducto = producto.getNombre();
                String numRegistro = producto.getNumeroDeRegistro();
                String descripcion = producto.getDescripcion();
                String usos = producto.getUsos();
                String composicion =producto.getComposicion();
                String dosificacion = producto.getDosificacion();
                String contraindicaciones = producto.getContraindicaciones();
                String tiendas = "";


                List<Tienda> listatiendas = producto.getTiendas();

                for(Tienda tienda : listatiendas){
                    tiendas = tiendas + tienda.getNombre() + "\n" + "dirección: " + tienda.getDireccion() + "\n" + "teléfono: " + tienda.getTelefono()+ "\n\n";
                }

                nombreTitulo.setText(nombreProducto);

                Picasso.with(getApplicationContext()).load(api_url + producto.getImagen().substring(1)).into(imagenPro);

                lista_Exp = (ExpandableListView) findViewById(R.id.lista_expandible);

                infoProducto = TextoPrueba.getInfo(numRegistro,descripcion,composicion,dosificacion,usos, contraindicaciones, tiendas);

                lista = TextoPrueba.getTitulo();
                adapter = new DescripcionAdapter(context, infoProducto, lista);
                lista_Exp.setAdapter(adapter);



            }

            @Override
            public void onFailure(Call<Producto> call, Throwable t) {
                //Log.i("error ", t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


        //lista expandible




        home.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), homeActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        back.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                finish();


            }
        });


        //Listening to button event
        tips.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), tipsActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        //Listening to button event
        laboratorio.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), listaLaboratorioActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });


    }


}
