package com.vivenat.juan.vivenat.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Producto {
    @SerializedName("laboratorio")
    @Expose
    private String laboratorio;

    @SerializedName("tiendas")
    @Expose
    private List<Tienda> tiendas;

    @SerializedName("tipoproducto")
    @Expose
    private String tipoproducto;

    @SerializedName("usos")
    @Expose
    private String usos;

    @SerializedName("composicion")
    @Expose
    private String composicion;

    @SerializedName("dosificacion")
    @Expose
    private String dosificacion;

    @SerializedName("presentacion")
    @Expose
    private String presentacion;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("nombre")
    @Expose
    private String nombre;

    @SerializedName("descripcion")
    @Expose
    private String descripcion;

    @SerializedName("numeroDeRegistro")
    @Expose
    private String numeroDeRegistro;

    @SerializedName("contraindicaciones")
    @Expose
    private String contraindicaciones;

    @SerializedName("imagen")
    @Expose
    private String imagen;

    public Producto(){}

    public Producto(String laboratorio, String tipoproducto, String usos, String composicion, String dosificacion, String presentacion,  String id, String nombre, String descripcion, String numeroDeRegistro, String imagen, String contraindicaciones) {
        this.laboratorio = laboratorio;
        this.tipoproducto = tipoproducto;
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.usos = usos;
        this.composicion = composicion;
        this.dosificacion = dosificacion;
        this.presentacion = presentacion;
        this.numeroDeRegistro = numeroDeRegistro;
        this.contraindicaciones = contraindicaciones;
        this.imagen = imagen;
    }


    public  String getComposicion()
    {
        return composicion;
    }

    public void setComposicion(String composicion)
    {
        this.composicion = composicion;
    }


    public  String getPresentacion()
    {
        return presentacion;
    }

    public void setPresentacion(String presentacion)
    {
        this.presentacion = presentacion;
    }

    public  String getDosificacion()
    {
        return dosificacion;
    }

    public void setDosificacion(String dosificacion)
    {
        this.dosificacion = dosificacion;
    }


    public String getLaboratorio()
    {
        return laboratorio;
    }

    public void setLaboratorio(String laboratorio)
    {
        this.laboratorio = laboratorio;
    }

    public String getTipoproducto()
    {
        return tipoproducto;
    }

    public void setTipoproducto(String tipoproducto)
    {
        this.tipoproducto = tipoproducto;
    }

    public String getUsos()
    {
        return usos;
    }

    public void setUsos(String usos)
    {
        this.usos = usos;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getNumeroDeRegistro()
    {
        return numeroDeRegistro;
    }

    public void setNumeroDeRegistro(String numeroDeRegistro) {
        this.numeroDeRegistro = numeroDeRegistro;
    }
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getImagen()
    {
        return imagen;
    }

    public void setImagen(String imagen)
    {
        this.imagen = imagen;
    }


    public String getContraindicaciones() {
        return contraindicaciones;
    }

    public void setContraindicaciones(String contraindicaciones) {
        this.contraindicaciones = contraindicaciones;
    }


    public List<Tienda> getTiendas() {
        return tiendas;
    }

    public void setTiendas(List<Tienda> tiendas) {
        this.tiendas = tiendas;
    }
}
