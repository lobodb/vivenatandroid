package com.vivenat.juan.vivenat;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class homeActivity extends AppCompatActivity {

    String textoabuscar;
    EditText busqueda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final Button busquedaBut = (Button) findViewById(R.id.buscar);

        ImageButton fitomedBut = (ImageButton) findViewById(R.id.fitomed);
        ImageButton bellezaBut = (ImageButton) findViewById(R.id.belleza);
        ImageButton suplemBut = (ImageButton) findViewById(R.id.suplemento);
        ImageButton esenciasBut = (ImageButton) findViewById(R.id.esencia);
        busqueda = (EditText) findViewById(R.id.inputSearch);

        final LinearLayout barrainferior = (LinearLayout) findViewById(R.id.bottomTabBar);

        ImageView tips = (ImageView) findViewById(R.id.tips);
        ImageView laboratorio = (ImageView) findViewById(R.id.laboratorio);
        //ImageView test = (ImageView) findViewById(R.id.test);

        //Listening to button event
        tips.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), tipsActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        //Listening to button event
        laboratorio.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), listaLaboratorioActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        /*Listening to button event
        test.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), testActivity.class);

                startActivity(nextScreen);

            }
        });
        */

        busqueda.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // do your stuff here
                    busquedaBut.callOnClick();
                }
                return false;
            }
        });
        //Listening to button event
        fitomedBut.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), tipoproductoActivity.class);
                nextScreen.putExtra("tipoproducto", "Fitomedicamento");
                nextScreen.putExtra("titulo","Búsqueda por Fitomedicamentos");
                startActivity(nextScreen);

            }
        });

        bellezaBut.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), tipoproductoActivity.class);
                nextScreen.putExtra("tipoproducto", "Cosmetico");
                nextScreen.putExtra("titulo","Búsqueda por Cosméticos");
                startActivity(nextScreen);

            }
        });

        suplemBut.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), tipoproductoActivity.class);
                nextScreen.putExtra("tipoproducto", "Suplemento");
                nextScreen.putExtra("titulo","Búsqueda por Suplementos");
                startActivity(nextScreen);

            }
        });

        esenciasBut.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), tipoproductoActivity.class);
                nextScreen.putExtra("tipoproducto", "Esencia");
                nextScreen.putExtra("titulo","Búsqueda por Esencias");
                startActivity(nextScreen);
            }
        });

        busquedaBut.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {


                textoabuscar = busqueda.getText().toString();
                if (textoabuscar!=null)
                {
                Intent nextScreen = new Intent(getApplicationContext(), busquedaActivity.class);
                nextScreen.putExtra("busqueda", textoabuscar);
                nextScreen.putExtra("titulo","Búsqueda General");
                startActivity(nextScreen);
                }
                else
                    Toast.makeText(getApplicationContext(), "El campo buscar no puede ir vacío", Toast.LENGTH_LONG).show();

            }
        });

    }

}