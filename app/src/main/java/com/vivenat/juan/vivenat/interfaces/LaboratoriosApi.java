package com.vivenat.juan.vivenat.interfaces;

import com.vivenat.juan.vivenat.models.Laboratorio;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


/**
 * Created by Juan on 9/12/16.
 */
public interface LaboratoriosApi {
    @GET("laboratorios.json")
    Call<List<Laboratorio>> getLaboratorios();

    @GET("laboratorio/{id_lab}.json")
    Call<Laboratorio> getLaboratorio(@Path("id_lab") String id_lab);

}
