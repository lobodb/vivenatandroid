package com.vivenat.juan.vivenat.utilities;

import com.vivenat.juan.vivenat.models.Laboratorio;
import com.vivenat.juan.vivenat.models.Producto;
import java.util.List;

/**
 * Created by Juan on 9/26/16.
 */

public class Paginador {

    public static final int ITEMS_PER_PAGE = 5;


    public List<Producto> generarPaginaProducto(int currentpage, List<Producto> productos) {

        int startitem = currentpage * ITEMS_PER_PAGE;
        List<Producto> productosreturn;

        if((startitem + 5) <= productos.size()) {
            productosreturn = productos.subList(startitem, startitem + 5);
        }else if(productos.size() <= ITEMS_PER_PAGE) {
            return productos;
        }
        else{
            productosreturn = productos.subList(startitem, productos.size());
        }

        return productosreturn;
    }

    public List<Laboratorio> generarPaginaLaboratorio(int currentpage, List<Laboratorio> laboratorios) {

        int startitem = currentpage * ITEMS_PER_PAGE;
        List<Laboratorio> laboratoriosreturn;

        if((startitem + 5) <= laboratorios.size()) {
            //antes estaba: laboratoriosreturn = laboratorios.subList(startitem, startitem + 5);
            laboratoriosreturn = laboratorios.subList(startitem, startitem + 5);
        }else if(laboratorios.size() <= ITEMS_PER_PAGE) {
            return laboratorios;
        }
        else{
            laboratoriosreturn = laboratorios.subList(startitem, laboratorios.size());
        }

        return laboratoriosreturn;
    }


}