package com.vivenat.juan.vivenat;

import android.app.ListActivity;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.vivenat.juan.vivenat.interfaces.ProductosApi;
import com.vivenat.juan.vivenat.models.Producto;
import com.vivenat.juan.vivenat.models.Productos;
import com.vivenat.juan.vivenat.utilities.Paginador;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class laboratorioSingularActivity extends ListActivity {

    //dirección
    private String api_url;

    public Productos productos = new Productos();

    //id del laboratorio que se va a mostrar
    private String id_lab;
    private String nombre_lab;
    ListView productosLV;
    public List<Producto> productosquellegan;
    private int pagina = 0;
    private int numeropaginas = 0;
    static ImageButton atras;
    static ImageButton adelante;


    public Producto producto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laboratorio_singular);



        //id del laboratorio del laboratorio que se va a mostrar
        id_lab = getIntent().getExtras().getString("id_lab");
        nombre_lab = getIntent().getExtras().getString("nombre_lab");

        final TextView nombre = (TextView) findViewById(R.id.nameLab);
        nombre.setText(nombre_lab);

        //dirección
        api_url = getResources().getString(R.string.api_url);

        //se crea archivo para extraer el json.
        Gson gson = new GsonBuilder().create();

        //se hace la petición.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api_url)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();

        ProductosApi productosApi = retrofit.create(ProductosApi.class);

        // extrae el Laboratorio
        final Call<List<Producto>> productoCall = (Call<List<Producto>>) productosApi.getProductos(id_lab);

        //encolar, para adaptar al laboratorio. o error por tiempo.
        productoCall.enqueue(new Callback<List<Producto>>() {


            @Override
            public void onResponse(Call<List<Producto>> call, Response<List<Producto>> response) {
                Paginador paginador = new Paginador();
                productosquellegan = response.body();

                if (productosquellegan!=null) {
                    numeropaginas = calculadornumeropaginas(response.body().size());
                    ToggleButtonsNavigation(0, numeropaginas);
                    List<Producto> productosamostrar = paginador.generarPaginaProducto(0, response.body());
                    productos.setProductos(productosamostrar);
                    productosLV = (ListView) getListView();
                    ArrayAdapter<Producto> adapter = new laboratorioSingularActivity.productosListAdapter();
                    productosLV.setAdapter(adapter);
                }
                Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();


            }

            @Override
            public void onFailure(Call<List<Producto>> call, Throwable t) {
               // Log.i("error ", t.getMessage());
                Toast.makeText(getApplicationContext(), "No se pudo enlazar con el servidor. Verifica tu conexión", Toast.LENGTH_LONG).show();
            }
        });



        String[] columnasBD = new String[] {"_id", "imagen", "textoSuperior", "textoInferior"};
        MatrixCursor cursor = new MatrixCursor(columnasBD);
        cursor.addRow(new Object[] {"0", R.drawable.artrifit, "Fitomedics", "texto 3."});

        //Añadimos los datos al Adapter y le indicamos donde dibujar cada dato en la fila del Layout
        /*String[] desdeEstasColumnas = {"imagen", "textoSuperior", "textoInferior"};
        int[] aEstasViews = {R.id.imageView_imagen, R.id.textView_nombre, R.id.textView_descripcion};
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.plantilla_laboratorio, cursor, desdeEstasColumnas, aEstasViews, 0);

        ListView listado = getListView();
        listado.setAdapter(adapter);*/


        //Botonera de la barra inferior.
        ImageView home = (ImageView) findViewById(R.id.home);
        ImageView tips = (ImageView) findViewById(R.id.tips);
        ImageView back = (ImageView) findViewById(R.id.atras);

        atras = (ImageButton) findViewById(R.id.botonAtras);
        adelante = (ImageButton) findViewById(R.id.botonAdelante);
        atras.setEnabled(false);
        adelante.setEnabled(false);

        tips.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), tipsActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        home.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), homeActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        back.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                finish();


            }
        });



        atras.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Paginador paginador = new Paginador();
                pagina = pagina - 1;
                ToggleButtonsNavigation(pagina, numeropaginas);
                Log.i("productos que llegan ",productosquellegan.size()+"");
                List<Producto> productosamostrar = paginador.generarPaginaProducto(pagina,productosquellegan);
                productos.setProductos(productosamostrar) ;
                productosLV = (ListView) getListView();
                ArrayAdapter<Producto> adapter = new laboratorioSingularActivity.productosListAdapter();
                productosLV.setAdapter(adapter);
            }
        });

        adelante.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Paginador paginador = new Paginador();
                pagina = pagina + 1;
                ToggleButtonsNavigation(pagina, numeropaginas);
                List<Producto> productosamostrar = paginador.generarPaginaProducto(pagina,productosquellegan);
                productos.setProductos(productosamostrar) ;
                productosLV = (ListView) getListView();
                ArrayAdapter<Producto> adapter = new laboratorioSingularActivity.productosListAdapter();
                productosLV.setAdapter(adapter);
            }
        });


        /*test.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), testActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });
        */

    }

    public static int calculadornumeropaginas(int entrada){
        int numeropaginas = 0;

        if(entrada>5){
            if(entrada%5 > 0){
                numeropaginas = ( entrada/5 ) + 1;
            }
            else{
                numeropaginas = ( entrada/5 );
            }

        }
        else{
            return 1;
        }



        return numeropaginas;
    }

    public static void ToggleButtonsNavigation(int pagina, int cantitaddepaginas){
        Log.i( "pagina", pagina + "" );
        if(pagina == 0) {
            atras.setEnabled(false);
        }else{
            atras.setEnabled(true);
        }

        if( (pagina + 1) == cantitaddepaginas ){
            adelante.setEnabled(false);
        }else{
            adelante.setEnabled(true);
        }
    }

    @Override
    public void onListItemClick(ListView lista, View view, int posicion, long id) {
        TextView textoTitulo = (TextView) view.findViewById(R.id.textView_nombre);

        Producto producto = productos.getProductos().get(posicion);

        //Toast.makeText(getApplicationContext(), texto, Toast.LENGTH_LONG).show();*/
        Intent nextScreen = new Intent(getApplicationContext(), productosActivity.class);
        nextScreen.putExtra("id_prod", producto.getId());

        Log.i("id_prod", producto.getId());


        startActivity(nextScreen);
    }



    //custom adapter.
    private class productosListAdapter extends ArrayAdapter<Producto> {
        public productosListAdapter() {
            super(laboratorioSingularActivity.this, R.layout.plantilla_producto, productos.getProductos());
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.plantilla_producto, parent, false);
            Producto productoActual = productos.getProductos().get(position);

            TextView name = (TextView) view.findViewById(R.id.textView_nombre);
            name.setText(productoActual.getNombre());

            TextView registro = (TextView) view.findViewById(R.id.textView_registro);
            registro.setText(productoActual.getNumeroDeRegistro());

            TextView attention = (TextView) view.findViewById(R.id.textView_usos);
            attention.setText(productoActual.getUsos());

            ImageView image = (ImageView) view.findViewById(R.id.imageView_imagen);


            Picasso.with(getApplicationContext()).load(api_url + productoActual.getImagen().substring(1)).into(image);

            return view;
        }
    }





}
