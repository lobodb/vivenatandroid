package com.vivenat.juan.vivenat.utilities;

import android.content.Context;
import android.content.SearchRecentSuggestionsProvider;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.vivenat.juan.vivenat.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DescripcionAdapter extends BaseExpandableListAdapter {

    private Context ctx;
    private ArrayList<String> descripcionCompleta;
    private ArrayList<String> listaDescripcion;

    public DescripcionAdapter(Context ctx, ArrayList<String> descripcionCompleta, ArrayList <String> listaDescripcion) {
        this.ctx=ctx;
        this.descripcionCompleta=descripcionCompleta;
        this.listaDescripcion=listaDescripcion;
    }

    @Override
    public int getGroupCount() {
        return descripcionCompleta.size();
    }

    @Override
    public int getChildrenCount(int i) {
        //TODO:cambiar para obtener lista de tiendas
        return 1 ;
    }

    @Override
    public Object getGroup(int i)
    {
        return listaDescripcion.get(i);
    }

    @Override
    public Object getChild(int parent, int child) {
        return descripcionCompleta.get(parent);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int parent, int child) {
        return child;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int parent, boolean isExpanded, View convertView, ViewGroup parentView) {

        String group_title = (String) getGroup(parent);

        if (convertView == null) {
            LayoutInflater inflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflator.inflate(R.layout.plantilla_listaextend_padre, parentView, false);
        }


        TextView parent_TextView = (TextView) convertView.findViewById(R.id.titulo_listaExpandible);
        //parent_TextView.setTypeface(null, Typeface.BOLD);
        parent_TextView.setText(group_title);

        return convertView;
    }

    @Override
    public View getChildView(int parent, int child, boolean lastChild, View convertView, ViewGroup parentView) {

        String child_title = (String) getChild(parent, child);

        if (convertView == null) {
            LayoutInflater inflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflator.inflate(R.layout.plantilla_listaextend_hijo, parentView, false);
        }


        TextView child_TextView = (TextView) convertView.findViewById(R.id.info_listaExpandible);
        child_TextView.setText(child_title);
        //parent_TextView.setTypeface(null, Typeface.BOLD);
        //parent_TextView.setText("emmm");

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}