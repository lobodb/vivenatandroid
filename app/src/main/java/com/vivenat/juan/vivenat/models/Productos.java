package com.vivenat.juan.vivenat.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Juan on 9/12/16.
 */
public class Productos {
    @SerializedName("productos")
    @Expose
    private List<Producto> productos;

    public Productos() {
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }


}
