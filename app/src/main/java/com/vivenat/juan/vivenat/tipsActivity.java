package com.vivenat.juan.vivenat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vivenat.juan.vivenat.interfaces.LaboratoriosApi;
import com.vivenat.juan.vivenat.interfaces.TipsApi;
import com.vivenat.juan.vivenat.models.Laboratorio;
import com.vivenat.juan.vivenat.models.Tip;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class tipsActivity extends AppCompatActivity {

    TextView imei;
    private String api_url;
    TextView tipstext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);

        imei = (TextView) findViewById(R.id.usuario);
        /*StringBuilder sb = new StringBuilder();
        TelephonyManager telMan = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        sb.append("Imei: "+telMan.getDeviceId()+"\n");*/

        imei.setText("¿Sabías Qué?");
        tipstext = (TextView) findViewById(R.id.tipsText);



        //dirección
        api_url = getResources().getString(R.string.api_url);


        //se crea archivo para extraer el json.
        Gson gson = new GsonBuilder().create();

        //se hace la petición.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api_url)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();


        TipsApi tipsApi = retrofit.create(TipsApi.class);

        // extrae lista Laboratorios
        final Call<Tip> tipCall = (Call<Tip>) tipsApi.getTip();


        //encolar, para adaptar a lista de laboratorio. o error por tiempo.
        tipCall.enqueue(new Callback<Tip>() {


            @Override
            public void onResponse(Call<Tip> call, Response<Tip> response) {
                tipstext.setText(response.body().getTexto());
            }

            @Override
            public void onFailure(Call<Tip> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "No se pudo enlazar con el servidor. Verifica tu conexión", Toast.LENGTH_LONG).show();
            }
        });


        ImageView home = (ImageView) findViewById(R.id.home);
        ImageView laboratorio = (ImageView) findViewById(R.id.laboratorio);
        //ImageView test = (ImageView) findViewById(R.id.test);
        ImageView vermas = (ImageView) findViewById(R.id.verMas);




        //Listening to button event
        home.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), homeActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        //Listening to button event
        laboratorio.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), listaLaboratorioActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        vermas.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                //Intent nextScreen = new Intent(getApplicationContext(), listaLaboratorioActivity.class);
                //nextScreen.putExtra("nombre", "Tip Product");
                //finish();
                //startActivity(nextScreen);

            }
        });


        /*Listening to button event
        test.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), testActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });
        */

    }
}
