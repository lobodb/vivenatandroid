package com.vivenat.juan.vivenat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class testActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        ImageView home = (ImageView) findViewById(R.id.home);
        ImageView tips = (ImageView) findViewById(R.id.tips);
        ImageView laboratorio = (ImageView) findViewById(R.id.laboratorio);




        //Listening to button event
        tips.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), tipsActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        //Listening to button event
        home.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), homeActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        //Listening to button event
        laboratorio.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), listaLaboratorioActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });


    }
}
