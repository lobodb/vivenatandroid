package com.vivenat.juan.vivenat.interfaces;

import com.vivenat.juan.vivenat.models.Producto;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface ProductosApi {

    @GET("productos/{id_lab}.json")
    Call<List<Producto>> getProductos(@Path("id_lab") String id_lab);

    @GET("productos/busqueda/{busqueda}.json")
    Call<List<Producto>> getProductosbusqueda(@Path("busqueda") String busqueda);

    @GET("productos/tipo/{tipo_producto}.json")
    Call<List<Producto>> getProductostipo(@Path("tipo_producto") String tipo_producto);

    @GET("producto/{id_prod}.json")
    Call<Producto> getProducto(@Path("id_prod") String id_prod);

}
