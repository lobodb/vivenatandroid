package com.vivenat.juan.vivenat;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.vivenat.juan.vivenat.interfaces.ProductosApi;
import com.vivenat.juan.vivenat.models.Producto;
import com.vivenat.juan.vivenat.models.Productos;
import com.vivenat.juan.vivenat.utilities.Paginador;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class tipoproductoActivity extends ListActivity {
    public Productos productos = new Productos();
    public List<Producto> productosquellegan;
    private ListView productosLV;
    private String api_url;
    private String tipoproducto;
    private int pagina = 0;
    private int numeropaginas = 0;
    static ImageButton atras;
    static ImageButton adelante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);

        atras = (ImageButton) findViewById(R.id.botonAtras);
        adelante = (ImageButton) findViewById(R.id.botonAdelante);
        atras.setEnabled(false);
        adelante.setEnabled(false);


        TextView titulo = (TextView) findViewById(R.id.titulo);

        titulo.setText(getIntent().getExtras().getString("titulo"));




        api_url = getResources().getString(R.string.api_url);


        //se crea archivo para extraer el json.
        Gson gson = new GsonBuilder().create();

        //se hace la petición.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api_url)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();


        ProductosApi productosApi = retrofit.create(ProductosApi.class);

        tipoproducto = getIntent().getExtras().getString("tipoproducto");

        // extrae lista Laboratorios
        final Call<List<Producto>> productoCall = (Call<List<Producto>>) productosApi.getProductostipo(tipoproducto);


        //encolar, para adaptar a lista de laboratorio. o error por tiempo.
        productoCall.enqueue(new Callback<List<Producto>>() {


            @Override
            public void onResponse(Call<List<Producto>> call, Response<List<Producto>> response) {


                Paginador paginador = new Paginador();
                productosquellegan = response.body();
                if (productosquellegan.size() > 0 )
                {//Log.i("uthaeotuh taoe", productosquellegan.size()+"");
                numeropaginas = calculadornumeropaginas(response.body().size());
                    Log.i("emm", productosquellegan.size()+"");
                ToggleButtonsNavigation(0, numeropaginas);
                List<Producto> productosamostrar = paginador.generarPaginaProducto(0,response.body());
                productos.setProductos(productosamostrar) ;
                productosLV = (ListView) getListView();
                ArrayAdapter<Producto> adapter = new tipoproductoActivity.productoListAdapter();
                productosLV.setAdapter(adapter);
                }

                else
                {
                    Toast.makeText(getApplicationContext(), "No hay productos en el momento en esta categoría, intenta más tarde", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<List<Producto>> call, Throwable t) {
                //Log.i("error ", t.getMessage());

                Toast.makeText(getApplicationContext(), "No se pudo enlazar con el servidor. Verifica tu conexión", Toast.LENGTH_LONG).show();
            }
        });


        ImageView home = (ImageView) findViewById(R.id.home);
        ImageView tips = (ImageView) findViewById(R.id.tips);
        ImageView lab = (ImageView) findViewById(R.id.laboratorio);
        ImageView back = (ImageView) findViewById(R.id.atras);



       atras.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Paginador paginador = new Paginador();
                pagina = pagina - 1;
                ToggleButtonsNavigation(pagina, numeropaginas);
                Log.i("productos que llegan ",productosquellegan.size()+"");
                List<Producto> productosamostrar = paginador.generarPaginaProducto(pagina,productosquellegan);
                productos.setProductos(productosamostrar) ;
                productosLV = (ListView) getListView();
                ArrayAdapter<Producto> adapter = new tipoproductoActivity.productoListAdapter();
                productosLV.setAdapter(adapter);
            }
        });

        adelante.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Paginador paginador = new Paginador();
                pagina = pagina + 1;
                ToggleButtonsNavigation(pagina, numeropaginas);
                List<Producto> productosamostrar = paginador.generarPaginaProducto(pagina,productosquellegan);
                productos.setProductos(productosamostrar) ;
                productosLV = (ListView) getListView();
                ArrayAdapter<Producto> adapter = new tipoproductoActivity.productoListAdapter();
                productosLV.setAdapter(adapter);
            }
        });


        //Botonera de la barra inferior.
        tips.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), tipsActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });


        home.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), homeActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        lab.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), listaLaboratorioActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        back.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), homeActivity.class);
                finish();
                startActivity(nextScreen);

            }
        });

        /*test.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent nextScreen = new Intent(getApplicationContext(), testActivity.class);
                finish();
                startActivity(nextScreen);

            }

        });
        */


    }

    public static int calculadornumeropaginas(int entrada){
        int numeropaginas = 0;

        if(entrada>5){
            if(entrada%5 > 0){
                numeropaginas = ( entrada/5 ) + 1;
            }
            else{
                numeropaginas = ( entrada/5 );
            }

        }
        else{
            return 1;
        }



        return numeropaginas;
    }

    public static void ToggleButtonsNavigation(int pagina, int cantitaddepaginas){
        Log.i( "pagina", pagina + "" );
        if(pagina == 0) {
            atras.setEnabled(false);
        }else{
            atras.setEnabled(true);
        }

        if( (pagina + 1) == cantitaddepaginas ){
            adelante.setEnabled(false);
        }else{
            adelante.setEnabled(true);
        }
    }



    @Override
    public void onListItemClick(ListView lista, View view, int posicion, long id) {
        Intent nextScreen = new Intent(getApplicationContext(), productosActivity.class);
        nextScreen.putExtra("id_prod", productos.getProductos().get(posicion).getId());
        nextScreen.putExtra("nombre_prod", productos.getProductos().get(posicion).getNombre());
        startActivity(nextScreen);
    }



    private class productoListAdapter extends ArrayAdapter<Producto> {



        public productoListAdapter() {
            super(tipoproductoActivity.this, R.layout.plantilla_producto, productos.getProductos());

        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            view = getLayoutInflater().inflate(R.layout.plantilla_producto, parent, false);
            Producto productoActual = productos.getProductos().get(position);

            TextView name = (TextView) view.findViewById(R.id.textView_nombre);
            name.setText(productoActual.getNombre());

            TextView registro = (TextView) view.findViewById(R.id.textView_registro);
            registro.setText(productoActual.getNumeroDeRegistro());

            TextView usos = (TextView) view.findViewById(R.id.textView_usos);
            usos.setText(productoActual.getUsos());


            ImageView image = (ImageView) view.findViewById(R.id.imageView_imagen);
            //image.setImageResource(R.drawable.artrifit);

            Picasso.with(getApplicationContext()).load(api_url + productoActual.getImagen().substring(1)).into(image);

            return view;
        }
    }
}
