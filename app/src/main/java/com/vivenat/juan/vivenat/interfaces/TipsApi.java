package com.vivenat.juan.vivenat.interfaces;

import com.vivenat.juan.vivenat.models.Laboratorio;
import com.vivenat.juan.vivenat.models.Tip;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Juan on 9/29/16.
 */

public interface TipsApi {
    @GET("consejo.json")
    Call<Tip> getTip();
}
