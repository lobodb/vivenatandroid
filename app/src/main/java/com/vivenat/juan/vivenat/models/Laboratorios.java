package com.vivenat.juan.vivenat.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Juan on 9/12/16.
 */
public class Laboratorios {
    @SerializedName("laboratorios")
    @Expose
    private List<Laboratorio> laboratorios;

    public Laboratorios() {
    }

    public List<Laboratorio> getLaboratorios() {
        return laboratorios;
    }

    public void setLaboratorios(List<Laboratorio> laboratorios) {
        this.laboratorios = laboratorios;
    }


}
