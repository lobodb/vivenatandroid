package com.vivenat.juan.vivenat.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Juan on 9/29/16.
 */

public class Tip {
    @SerializedName("producto")
    @Expose
    private String producto;

    @SerializedName("texto")
    @Expose
    private String texto;

    public Tip(){}

    public Tip(String producto, String texto){
        this.producto = producto;
        this.texto = texto;
    }


    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
